/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : one_top_service

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2018-09-13 11:58:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `forum_id` int(50) DEFAULT NULL COMMENT '论坛id',
  `type` char(2) COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(2000) COLLATE utf8_bin DEFAULT NULL COMMENT '内容',
  `create_by` int(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(6) DEFAULT NULL,
  `enabled` char(2) COLLATE utf8_bin DEFAULT NULL,
  `version` char(2) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for forum
-- ----------------------------
DROP TABLE IF EXISTS `forum`;
CREATE TABLE `forum` (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '创建人id',
  `type` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT '类型，没有实际意义，为了方便以后拓展',
  `content` varchar(2000) COLLATE utf8_bin DEFAULT NULL COMMENT '论坛内容',
  `is_pic` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT '是否有图片',
  `pic_url` varchar(2000) COLLATE utf8_bin DEFAULT NULL COMMENT '图片地址',
  `create_by` int(50) DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `enabled` char(2) COLLATE utf8_bin DEFAULT '1' COMMENT '是否有效1.有效，0.无效，拓展用',
  `version` char(2) COLLATE utf8_bin DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `sex` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT '0.男 1.女',
  `age` int(10) DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '居住地址',
  `mail_address` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '邮箱地址',
  `description` varchar(400) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `head_portrait_url` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '头像路径',
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `version` varchar(2) COLLATE utf8_bin DEFAULT '1' COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
