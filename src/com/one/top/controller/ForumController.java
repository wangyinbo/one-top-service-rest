package com.one.top.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.one.top.entity.Forum;
import com.one.top.service.ForumService;

/**
 * 操作对象的控制层代码
 * 
 * @author 
 */
@Controller
@RequestMapping("/forum")
public class ForumController {
	/**
	 * 业务层接口
	 */
	@Autowired
	@Qualifier("forumService")
	private ForumService forumService;

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String addEntity(@RequestBody Forum entity) {
		// 调用业务层接口添加实体对象
		forumService.addEntity(entity);
		// 返回增加操作结果
		return "Add success!";
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String deleteEntity(@PathVariable Integer id) {
		// 调用业务层接口删除实体对象
		forumService.deleteEntity(id);
		// 返回删除操作结果
		return "Delete success!";
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateEntity(@RequestBody Forum entity) {
		// 调用业务层接口修改实体对象
		forumService.updateEntity(entity);
		// 返回修改操作结果
		return "Update success!";
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	@RequestMapping(value = "/read/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Forum readEntity(@PathVariable Integer id) {
		// 调用业务层接口查询实体对象
		return forumService.readEntity(id);
	}

	/**
	 * 查询所有实体对象
	 */
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	@ResponseBody
	public List<Forum> readEntities() {
		// 调用业务层接口查询实体对象集合
		return forumService.readEntities();
	}
}