package com.one.top.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.one.top.entity.Comment;
import com.one.top.service.CommentService;


/**
 * 操作对象的控制层代码
 * 
 * @author 
 */
@Controller
@RequestMapping("/comment")
public class CommentController {
	/**
	 * 业务层接口
	 */
	@Autowired
	@Qualifier("commentService")
	private CommentService commentService;

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String addEntity(@RequestBody Comment entity) {
		// 调用业务层接口添加实体对象
		commentService.addEntity(entity);
		// 返回增加操作结果
		return "Add success!";
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String deleteEntity(@PathVariable Integer id) {
		// 调用业务层接口删除实体对象
		commentService.deleteEntity(id);
		// 返回删除操作结果
		return "Delete success!";
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public String updateEntity() {
		//测试
		Comment entity=new Comment();
		System.out.println("....");
		// 调用业务层接口修改实体对象
		commentService.updateEntity(entity);
		// 返回修改操作结果
		return "comment/update_view";
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	@RequestMapping(value = "/read/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Comment readEntity(@PathVariable Integer id) {
		// 调用业务层接口查询实体对象
		return commentService.readEntity(id);
	}

}