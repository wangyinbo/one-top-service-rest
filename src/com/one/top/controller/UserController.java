package com.one.top.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSONObject;
import com.one.top.constants.ResultDataConstant;
import com.one.top.entity.BaseDate;
import com.one.top.entity.User;
import com.one.top.service.UserService;
import com.one.top.service.util.CacheManagerUtil;
import com.one.top.service.util.JacksonUtils;
import com.one.top.vo.ResultData;

/**
 * 操作对象的控制层代码
 * 
 * @author 
 */
@Controller
@RequestMapping("/user")
public class UserController {
	/**
	 * 业务层接口
	 */
	@Autowired
	@Qualifier("userService")
	private UserService userService;

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String addEntity(@RequestBody User entity) {
		// 调用业务层接口添加实体对象
		userService.addEntity(entity);
		// 返回增加操作结果
		return "Add success!";
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String deleteEntity(@PathVariable Integer id) {
		// 调用业务层接口删除实体对象
		userService.deleteEntity(id);
		// 返回删除操作结果
		return "Delete success!";
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateEntity(@RequestBody User entity) {
		// 调用业务层接口修改实体对象
		userService.updateEntity(entity);
		// 返回修改操作结果
		return "Update success!";
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	@RequestMapping(value = "/read/{id}", method = RequestMethod.GET)
	@ResponseBody
	public User readEntity(@PathVariable Integer id) {
		// 调用业务层接口查询实体对象
		return userService.readEntity(id);
	}

	/**
	 * 查询所有实体对象
	 */
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	@ResponseBody
	public List<User> readEntities() {
		// 调用业务层接口查询实体对象集合
		return userService.readEntities();
	}
	
	
	
	/**
	 * 用户登陆
	 * @param message
	 * @return
	 */
	@RequestMapping("/login")
	@ResponseBody
	public String Login(@RequestBody(required = false) String message , HttpServletRequest request) {
		
		ResultData rd=new ResultData();
		rd.setCode(ResultDataConstant.FILE_CODE);
		Map<String ,String> map=new HashMap<String ,String>();
		try {
			//消除空格
			if (message != null && !"".equals(message)) {
				message = message.replace(" ", "");
				 //转换成基类
				User user = JacksonUtils.jsonToObject(message, User.class);
				Vector<User> userList=userService.Login(user);
				//查询数据有效
				if(null!=userList&&userList.size()>0){
					//session添加token
					String token=user.getUserId()+Calendar.getInstance().getTimeInMillis();
					//添加缓存和时效时间
					CacheManagerUtil.set("token_"+user.getUserId(), token , 24*60*60*1000);
					map.put("token", token);
					map.put("userId", userList.get(0).getId().toString());
				}
				rd.setCode(ResultDataConstant.SUCCESS_CODE);
				rd.setMessage(map);
			}
		} catch (Exception e) {
			rd.setCode(ResultDataConstant.FILE_CODE);
			e.printStackTrace();	
		}
		
		return JSONObject.toJSONString(rd);

	}

	
}