package com.one.top.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.springframework.stereotype.Repository;

import com.one.top.dao.UserDao;
import com.one.top.entity.Comment;
import com.one.top.entity.User;
import com.one.top.service.util.DBUtil;


/**
 * 操作对象的持久层代码
 * 
 * @author 
 */
@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Override
	public void addEntity(User entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteEntity(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEntity(User entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User readEntity(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> readEntities() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	/**
	 * 用户登陆
	 * @param user
	 * @return
	 */
	@Override
	public Vector<User> Login(User user)  throws Exception{
		String sql="select * from  user  where id=? and pass_word=?";
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		Vector<User> pros=new Vector<User>();
		try{
			conn=DBUtil.getConnection();
			pst=(PreparedStatement)conn.prepareStatement(sql);
			pst.setString(1,user.getUserId().toString());
			pst.setString(2,user.getPassWord().toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				User s =new User(	
				rs.getInt(1),
				rs.getString(2),
				rs.getString(3),
				rs.getString(4),
				
				rs.getInt(5),
				rs.getString(6),
				rs.getString(7),
				
				rs.getString(8),
				rs.getString(9),
				rs.getDate(10),
				rs.getDate(11),
				rs.getString(12));
				pros.add(s);
			}
		}catch(Exception e) {
			throw new Exception();
		}finally {
			DBUtil.closeConnection(pst, conn);
		}
		return pros;
	}

}