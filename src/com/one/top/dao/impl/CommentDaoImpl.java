package com.one.top.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.one.top.dao.CommentDao;
import com.one.top.entity.Comment;
import com.one.top.service.util.DBUtil;

/**
 * 操作对象的持久层代码
 * 
 * @author 
 */
@Repository("commentDao")
public class CommentDaoImpl implements CommentDao {

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	public void addEntity(Comment entity) {
		Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn = DBUtil.getConnection();
          //TODO 这个sql需要修改
            stmt = (PreparedStatement)conn.prepareStatement("insert into student(class_number,name,age,sex,student_id,dormitory_number,telphone_number) values(?,?,?,?,?,?,?)");
           //TODO 这里需要将对象属性set进去
            stmt.setInt(1, entity.getForumId());
           
            stmt.executeUpdate();
        }catch(SQLException e) {
			e.printStackTrace();
        }finally{
        	DBUtil.closeConnection(stmt, conn);
        }
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	public void deleteEntity(Integer id) {
		Connection conn=null;
		PreparedStatement pst=null;


		conn=DBUtil.getConnection();
		//TODO 这个sql需要修改
		String sql="delete from  student where id=?";
		try {
			pst=(PreparedStatement)conn.prepareStatement(sql);
			pst.setString(1, id.toString());
			pst.executeUpdate();
		} catch (SQLException e) {
			System.out.print("删除失败");
			e.printStackTrace();
		}
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	public void updateEntity(Comment entity) {
		Connection conn=null;
		PreparedStatement pst=null;
		
		conn=DBUtil.getConnection();
		//TODO 这个sql需要修改
		String sql="update student set class_number=?,name=?,age=?,sex=?,student_id=?,dormitory_number=?,Telphone_number=? where id=?";
		try {
			pst=(PreparedStatement)conn.prepareStatement(sql);
			//TODO 这里需要将对象属性set进去
			pst.setInt(1, entity.getForumId());
		
			pst.executeUpdate();
		} catch (SQLException e) {
			System.out.print("修改信息失败");
			e.printStackTrace();
		}
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	public Comment readEntity(Integer id) {
		//TODO sql休要修改
		String sql="select * from  student where id=?";
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		Comment pros=new Comment();
		conn=DBUtil.getConnection();
		try{
			pst=(PreparedStatement)conn.prepareStatement(sql);
			pst.setString(1,id.toString());
			 rs = pst.executeQuery();
			while(rs.next()) {
//				Comment s =new Comment(
//				rs.getInt(1),
//				rs.getInt(2),
//				rs.getString(3),
//				rs.getString(4),
//				rs.getInt(5),
//				rs.getString(6),
//				rs.getInt(7),
//				rs.getString(8));
//				pros.add(s);
			}
		}
		catch(SQLException e) {
			System.out.println("SQL执行完毕");
		}
		finally {
			DBUtil.closeConnection(pst, conn);
		}
		return pros;
	}

}