package com.one.top.dao;

import java.util.List;

import com.one.top.entity.Forum;

/**
 * 持久层接口
 * 
 * @author 
 */
public interface ForumDao {
	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	void addEntity(Forum entity);

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	void deleteEntity(Integer id);

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	void updateEntity(Forum entity);

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	Forum readEntity(Integer id);

	/**
	 * 查询所有实体对象
	 */
	List<Forum> readEntities();
}