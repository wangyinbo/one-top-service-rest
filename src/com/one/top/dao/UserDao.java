package com.one.top.dao;

import java.util.List;
import java.util.Vector;

import com.one.top.entity.User;

/**
 * 持久层接口
 * 
 * @author 
 */
public interface UserDao {
	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	void addEntity(User entity);

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	void deleteEntity(Integer id);

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	void updateEntity(User entity);

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	User readEntity(Integer id);

	/**
	 * 查询所有实体对象
	 */
	List<User> readEntities();
	
	/**
	 * 用户登陆
	 * @param user
	 * @return
	 */
	public Vector<User> Login(User user)  throws Exception;
}