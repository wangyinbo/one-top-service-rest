package com.one.top.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * @author 
 */
public class Forum  extends BaseDate  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9045325462052876272L;

	/**
	 * 创建人id
	 */
	private Integer id;

	/**
	 * 类型，没有实际意义，为了方便以后拓展
	 */
	private String type;

	/**
	 * 论坛内容
	 */
	private String content;

	/**
	 * 是否有图片
	 */
	private String isPic;

	/**
	 * 图片地址
	 */
	private String picUrl;

	private Integer createBy;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 是否有效1.有效，0.无效，拓展用
	 */
	private String enabled;

	private String version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsPic() {
		return isPic;
	}

	public void setIsPic(String isPic) {
		this.isPic = isPic;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}