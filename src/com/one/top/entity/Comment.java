package com.one.top.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */

public class Comment extends BaseDate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7145646344730418646L;
	
	public Comment() {}

	public Comment(Integer id, Integer forumId, String type, String content,
			Integer createBy, Date createTime, String enabled, String version) {
		this.id = id;
		this.forumId = forumId;
		this.type = type;
		this.content = content;
		this.createBy = createBy;
		this.createTime = createTime;
		this.enabled = enabled;
		this.version = version;
	}

	private Integer id;

	/**
	 * 论坛id
	 */
	private Integer forumId;

	private String type;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 创建人
	 */
	private Integer createBy;

	private Date createTime;

	private String enabled;

	private String version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getForumId() {
		return forumId;
	}

	public void setForumId(Integer forumId) {
		this.forumId = forumId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", forumId=" + forumId + ", type=" + type
				+ ", content=" + content + ", createBy=" + createBy
				+ ", createTime=" + createTime + ", enabled=" + enabled
				+ ", version=" + version + "]";
	}
	
	
}