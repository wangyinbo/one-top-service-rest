package com.one.top.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * @author 
 */

public class User extends BaseDate  implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1311461469961093127L;
	
	public User(){}

	public User(Integer id, String passWord, String name, String sex,
			Integer age, String address, String mailAddress,
			String description, String headPortraitUrl, Date createTime,
			Date updateTime, String version) {
		super();
		this.id = id;
		this.passWord = passWord;
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.address = address;
		this.mailAddress = mailAddress;
		this.description = description;
		this.headPortraitUrl = headPortraitUrl;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.version = version;
	}
	private Integer id;

	private String passWord;
	
	private String name;

	/**
	 * 0.男 1.女
	 */
	private String sex;

	private Integer age;

	/**
	 * 居住地址
	 */
	private String address;

	/**
	 * 邮箱地址
	 */
	private String mailAddress;

	/**
	 * 描述
	 */
	private String description;

	/**
	 * 头像路径
	 */
	private String headPortraitUrl;

	private Date createTime;

	private Date updateTime;

	/**
	 * 版本
	 */
	private String version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHeadPortraitUrl() {
		return headPortraitUrl;
	}

	public void setHeadPortraitUrl(String headPortraitUrl) {
		this.headPortraitUrl = headPortraitUrl;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}