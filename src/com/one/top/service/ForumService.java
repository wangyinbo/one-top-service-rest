package com.one.top.service;

import java.util.List;

import com.one.top.entity.Forum;

/**
 * 业务层接口
 * 
 * @author 
 */
public interface ForumService {
	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	void addEntity(Forum entity);

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	void deleteEntity(Integer id);

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	void updateEntity(Forum entity);

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	Forum readEntity(Integer id);

	/**
	 * 查询所有实体对象
	 */
	List<Forum> readEntities();
}