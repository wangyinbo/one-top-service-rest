package com.one.top.service.util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class DBUtil {
	private static final Logger logger= Logger.getLogger(DBUtil.class);
	
	public static Connection getConnection() {
		Connection conn=null;
		String url="jdbc:mysql://127.0.0.1:3306/one_top_service?user=root&password=root&characterEncoding=utf-8";
		
			try {
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				
				conn=(Connection)DriverManager.getConnection(url);
			} catch (Exception e) {
				logger.error("打开数据库连接异常："+e.getMessage());
				e.printStackTrace();
			}
		return conn;
	}
	public static void closeConnection(ResultSet rs,Statement st,Connection conn) {
		try{
			if(rs!=null)
				rs.close();
			if(st!=null)
				st.close();
			if(conn!=null)
				conn.close();
		}
		catch(SQLException e) {
			logger.error("关闭数据库连接异常："+e.getMessage());
		}
	}
	
	
	public static void closeConnection(PreparedStatement  st,Connection conn) {
		try{
			if(st!=null)
				st.close();
			if(conn!=null)
				conn.close();
		}
		catch(SQLException e) {
			logger.error("关闭数据库连接异常："+e.getMessage());
		}
	}
}
