package com.one.top.service;

import com.one.top.entity.Comment;

/**
 * 业务层接口
 * 
 * @author 
 */
public interface CommentService {
	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	void addEntity(Comment entity);

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	void deleteEntity(Integer id);

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	void updateEntity(Comment entity);

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	Comment readEntity(Integer id);

}