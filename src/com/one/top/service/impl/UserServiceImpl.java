package com.one.top.service.impl;

import java.util.List;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.one.top.dao.UserDao;
import com.one.top.entity.User;
import com.one.top.service.UserService;

/**
 * 操作对象的业务层代码
 * 
 * @author 
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	/**
	 * 持久层接口
	 */
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	public void addEntity(User entity) {
		// 调用持久层接口添加实体对象
		userDao.addEntity(entity);
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	public void deleteEntity(Integer id) {
		// 调用持久层接口删除实体对象
		userDao.deleteEntity(id);
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	public void updateEntity(User entity) {
		// 调用持久层接口修改实体对象
		userDao.updateEntity(entity);
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	public User readEntity(Integer id) {
		// 调用持久层接口查询实体对象
		return userDao.readEntity(id);
	}

	/**
	 * 查询所有实体对象
	 */
	public List<User> readEntities() {
		// 调用持久层接口查询实体对象集合
		return userDao.readEntities();
	}
	
	/**
	 * 用户登陆
	 * @param user
	 * @return
	 */
	public Vector<User> Login(User user) throws Exception{
		
		return userDao.Login(user);
	}
}