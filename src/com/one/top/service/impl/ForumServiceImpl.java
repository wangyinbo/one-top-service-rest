package com.one.top.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.one.top.dao.ForumDao;
import com.one.top.entity.Forum;
import com.one.top.service.ForumService;

/**
 * 操作对象的业务层代码
 * 
 * @author 
 */
@Service("forumService")
@Transactional
public class ForumServiceImpl implements ForumService {
	/**
	 * 持久层接口
	 */
	@Autowired
	@Qualifier("forumDao")
	private ForumDao forumDao;

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	public void addEntity(Forum entity) {
		// 调用持久层接口添加实体对象
		forumDao.addEntity(entity);
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	public void deleteEntity(Integer id) {
		// 调用持久层接口删除实体对象
		forumDao.deleteEntity(id);
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	public void updateEntity(Forum entity) {
		// 调用持久层接口修改实体对象
		forumDao.updateEntity(entity);
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	public Forum readEntity(Integer id) {
		// 调用持久层接口查询实体对象
		return forumDao.readEntity(id);
	}

	/**
	 * 查询所有实体对象
	 */
	public List<Forum> readEntities() {
		// 调用持久层接口查询实体对象集合
		return forumDao.readEntities();
	}
}