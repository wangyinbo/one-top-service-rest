package com.one.top.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.one.top.dao.CommentDao;
import com.one.top.entity.Comment;
import com.one.top.service.CommentService;

/**
 * 操作对象的业务层代码
 * 
 * @author 
 */
@Service("commentService")
@Transactional
public class CommentServiceImpl implements CommentService {
	/**
	 * 持久层接口
	 */
	@Autowired
	@Qualifier("commentDao")
	private CommentDao commentDao;

	/**
	 * 增加实体对象
	 * @param entity 实体
	 */
	public void addEntity(Comment entity) {
		// 调用持久层接口添加实体对象
		commentDao.addEntity(entity);
	}

	/**
	 * 根据主键删除实体对象
	 * @param id 主键
	 */
	public void deleteEntity(Integer id) {
		// 调用持久层接口删除实体对象
		commentDao.deleteEntity(id);
	}

	/**
	 * 修改实体对象
	 * @param entity 实体
	 */
	public void updateEntity(Comment entity) {
		// 调用持久层接口修改实体对象
		commentDao.updateEntity(entity);
	}

	/**
	 * 根据主键查询实体对象
	 * @param id 主键
	 */
	public Comment readEntity(Integer id) {
		// 调用持久层接口查询实体对象
		return commentDao.readEntity(id);
	}

}